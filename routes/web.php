<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = [
        0 => [
            'id' => 1,
            'name' => 'Product 1',
        ],
        1 => [
            'id' => 2,
            'name' => 'Product 2',
        ],
    ];
    return view('welcome', compact('products'));
});

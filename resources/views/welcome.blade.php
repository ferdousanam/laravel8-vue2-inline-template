<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>
</head>
<body>
    <div id="app">
        <div class="container">
            <h1 style="text-align: center;">Laravel 8 & VueJS 2 Inline Template</h1>
            @foreach($products as $product)
                <example-component inline-template product="{{json_encode($product)}}">
                    <div>
                        <h1 class="center-heading" v-text="item.name">{{$product['name']}}</h1>

                        <h1 class="center-heading" v-text="counter">0</h1>
                        <div class="button-container">
                            <button class="btn" @click="increase">Increase</button>
                            <button class="btn" @click="decrease">Decrease</button>
                        </div>
                    </div>
                </example-component>
            @endforeach
        </div>
    </div>

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>
